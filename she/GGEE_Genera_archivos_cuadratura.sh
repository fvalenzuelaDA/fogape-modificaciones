#!/usr/bin/sh
#************************************************************************
# Nombre				: GGEE_Genera_archivos_cuadratura.sh
# Ruta					: /she/
# Autor					: Gabriel Martínez (Ada) - Ing. SW BCI: Daniel Araya
# Fecha					: 14/11/2019
# Descripción			: Ejecuta sql en Microsoft SQL Server
# Parámetros de Entrada	: SQL a ejecutar
# Ejemplo de ejecución	: sh GGEE_Genera_archivos_cuadratura.sh nombresql
#************************************************************************

sNOMBRE_SHELL="GGEE_Genera_archivos_cuadratura.sh"

sLOG=$HOME/log
sCFG=$HOME/cfg

printlog()
{
	Hora=`date +%H:%M:%S`
	echo ${Hora} "$@" # Salida a Consola
	echo ${Hora} "$@"  >> $sLOG/${SHELL_LOG}
}

SHELL_LOG=`date +'%Y%m%d'.GGEE_Genera_archivos_cuadratura.${1}.log`
ARCHLOG=`date +'%Y%m%d'.GGEE_Genera_archivos_cuadratura.${1}`
date '+  %Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG
server=`uname -n`
dia=`date +'%Y'/'%m'/'%d'`
Hora=`date +'%H':'%M':'%S'`
AMB=`BciAmbiente`
RutaAmbiente="$HOME/cfg/GGEE_ambiente.txt"
INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog "if [ $1 -gt 0 ]"
	if [ $1 -gt 0 ]
	then
		printlog "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
	fi
	printlog "************************************************************************************"
	printlog "**                     RESUMEN                                                    **"
	printlog "************************************************************************************"
	printlog "Servidor      : `uname -n`"
	printlog "Usuario       : `whoami`" 
	printlog "Programa      : ${INF_PGM}"
	printlog "ID proceso    : ${INF_PID}"
	printlog "Hora_Inicio   : ${INF_INI}"
	printlog "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog "Parametros    : ${INF_ARG}"
	printlog "Cant. Parametr: ${INF_CANT_ARG}"
	printlog "Resultado     : ${2} "
	printlog "Archivo Log   : ${sLOG}/${ARCHLOG}"
	printlog "************************************************************************************"
	exit $1
}


###################################################################
#					Ejecuta archivo ambiente  					  #
#	    Valida la existencia del archivo de Configuracion 		  #
###################################################################

printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaAmbiente ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaAmbiente"
	echo " "
	SALIR 2 "ERROR: No se encuentra el archivo $RutaAmbiente"
fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaAmbiente
then
	echo "Archivo $RutaAmbiente OK"
	. $RutaAmbiente
else
	echo "Error, Archivo sin datos: $RutaAmbiente"
	SALIR 2 "Error, Archivo sin datos: $RutaAmbiente"
fi
printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaFunc ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaFunc"
	echo " "
	SALIR 2 "ERROR: No se encuentra el archivo $RutaFunc"
fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaFunc
then
	echo "Archivo $RutaFunc OK"
	. $RutaFunc
else
	echo "Error, Archivo sin datos: $RutaFunc"
	SALIR 2 "Error, Archivo sin datos: $RutaFunc"
fi

printlog "Valida existencia de sub Directorio en out "
if [ ! -d $DirOutCua ]
then 
	echo " "
	echo "ERROR: No se encuentra el sub Directorio en out"
	echo " "
	SALIR 2 "ERROR: No se encuentra el sub Directorio en out"
fi

printlog "Valida existencia de sub Directorio en bkp "
if [ ! -d $DirBkpCua ]
then 
	echo " "
	echo "ERROR: No se encuentra el sub Directorio en bkp"
	echo " "
	SALIR 2 "ERROR: No se encuentra el sub Directorio en bkp"
fi


Eliminar_Archivo $DirLog/$SHELL_LOG
Eliminar_Archivo $DirLog/$ARCHLOG

sFecha=`date '+%d/%m/%Y'`
sHora=`date '+%X'`

printlog "if [ $# -eq 1 ]"
if [ $# -eq 1 ]
then
	Mensaje "Cantidad de parametros correcta"
	printlog "Cantidad de parametros correcta"
	NomSql=$1
else
	Mensaje "Cantidad de parametros incorrecta"
	printlog "Cantidad de parametros incorrecta"
	SALIR 2 "Cantidad de parametros incorrecta"
fi
	
#Comienza proceso
#modificaciones


archivo_cfg=${1}.txt
archivo_sql=${1}.sql


############################################################################
# Se realiza la conexion a la base de datos
############################################################################

Realiza_Conexion $UyPuser

############################################################################
#Comienza ejecucion de SQL para Preparar data para la generacion de archivos
############################################################################

printlog "Ejecucion de ${DirFue}/${archivo_sql}"
Mensaje "Ejecucion de ${DirFue}/${archivo_sql}"
Ejecuta_Query ${DirFue}/${archivo_sql}

Mensaje ""
Mensaje "Fin Ejecucion de ${DirFue}/${archivo_sql}"
date '+%Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG

estado=$?


#Validamos Ejecución del Modulo
if [ $estado -ne 0 ]
then
	printlog "Error en Modulo $archivo_sql"
	exit 2
fi
printlog "Ok Ejecucion Correcta Modulo $archivo_sql"


############################################################################
#obtengo numero de lineas del archivo de Configuración
############################################################################

Valida_Archivo $DirCfg/$archivo_cfg
N_Proc=`wc -l $DirCfg/$archivo_cfg | awk '{ printf "%i", $1 }'`

inicio=1
while [ $inicio -le $N_Proc ]; do

if [ $inicio -lt "10" ]
then
	indice=0$inicio
else
	indice=$inicio
fi

NombreProc=`awk '$1 ~ /'$indice'/ {print $2}' $DirCfg/$archivo_cfg`
TipoAlerta=`awk '$1 ~ /'$indice'/ {print $3}' $DirCfg/$archivo_cfg`
ArchSalida=`awk '$1 ~ /'$indice'/ {print $4}' $DirCfg/$archivo_cfg`
IndEjecProc=`awk '$1 ~ /'$indice'/ {print $5}' $DirCfg/$archivo_cfg`

############################################################################
#Se respaldan los archivos de la ejecucion anterior en el directorio bkp
############################################################################

Respaldo_Bkp ${DirOutCua}/${ArchSalida} ${DirBkpCua}

############################################################################
#Comienza a generar archivos por alertas
############################################################################


#Ejecución de Modulos *.sql que genera los archivos con detalle de cuadratura
if [ $IndEjecProc = 'S' ]
then
	
	estado2=$?
	
	# Genera los archivos con las alertas 
	sed 's/VarIndProc/'${TipoAlerta}'/g' $ArchDetalle > ${DirTmp}/Proceso_tmp.sql
	Ejecuta_Query_Archivo ${DirTmp}/Proceso_tmp.sql ${DirTmp}/Archivo_salida_tmp.csv
	cut -f 1-27 -d";" ${DirTmp}/Archivo_salida_tmp.csv > ${DirOutCua}/${ArchSalida}

	
	Mensaje ""
	Mensaje "Fin de la generacion de archivo ${ArchSalida}"
	date '+%Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG
	
	
	#Validamos Ejecución del Modulo
	if [ $estado2 -ne 0 ]
	then
		printlog "Error en creacion de archivo ${ArchSalida}"
		exit 2
	fi
	printlog "Archivo ${ArchSalida} creado Ok"

	Eliminar_Archivo ${DirTmp}/Prueba_sp1.sql
	Eliminar_Archivo ${DirTmp}/Archivo_salida_tmp.csv
	Eliminar_Archivo ${DirTmp}/Proceso_tmp.sql
fi


let inicio=inicio+1
done


printlog ""
printlog "Hora de Termino "$Hora
printlog " Fin Shell GGEE_Genera_archivos_cuadratura.sh"
SALIR 0 "OK"
