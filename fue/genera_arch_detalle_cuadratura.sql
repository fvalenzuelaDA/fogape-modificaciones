
--realiza consulta a la tabla que contiene el detalle con su indicador de alerta para generar archivo
select 
	detc_alerta as Nombre_Alerta
	,detc_ope_origen	as Num_Operacion_Origen
	,detc_ope_vig	as Num_Operacion_Vigente
	,detc_rut_cliente	as Rut_Cliente
	,detc_dv_cliente	as Digito_Verificador
	,detc_fec_curse	as Fecha_Curse
	,detc_moneda_ope	as Moneda_Operacion
	,cast(detc_monto_ope as decimal(18,0)) as Monto_Operacion
	,cast(detc_monto_garantizado as decimal(18,0)) as Monto_Garantizado
	,cast(detc_saldo_capital as decimal(18,0)) as Saldo_Operacion
	,detc_fec_vencimiento_final	as Fecha_Vcto_Final
	,detc_fec_prox_vencimiento	as Fecha_Prox_Vcto
	,detc_dias_mora	as Dias_Mora
	,detc_tio_aux	as Tio_Aux
	,detc_cond_garantia	as Condicion_Garantia
	,REPLACE(rtrim(cast(detc_tasa_comision as varchar(10))),'.', ',')	as Tasa_Comision
	,detc_tasa_garantia	as Tasa_Garantia
	,detc_nro_garantia	as Num_Garantia
	,detc_tipo_garantia	as Tipo_Garantia
	,detc_estado_garantia	as Estado_Garantia
	,detc_nro_gpc	as Num_GPC
	,detc_tipo_gpc	as Tipo_GPC
	,detc_monto_gpc	as Monto_GPC
	,REPLACE(detc_estado_formalizacion,'V', 'VIGENTE')	as Estado_Formalizacion
	,detc_nro_gar_safio	as Num_Garantia_Safio
	,detc_estado_fogape	as Estado_Fogape
	,detc_nro_licitacion	as Num_Licitacion
	,detc_ind_alerta	
from 
	det_cuadratura
where 
	detc_ind_alerta = VarIndProc

go
